<form>
    <div class="container-fluid">
        <div class="row pt-3">
            <div class="col-3">
                <label for="lado" class="form-label">Lado: </label>
            </div>

            <div class="col-9">
                <input type="number" class="form-control" id="correo" placeholder="Introduce un numero" name="lado">
            </div>
        </div>
        <div class="row pt-3">
            <div class="col-3">
                <label for="radio" class="form-label">Radio: </label>
            </div>

            <div class="col-9">
                <input type="number" class="form-control" id="contra" placeholder="Introduce un numero" name="radio">

            </div>
        </div> 

        <div class="form-group row mt-2">
            <div class="col-lg-2">
                <button name="cuadrado" class="btn btn-primary col-lg-8">Cuadrado</button>
            </div>
            <div class="col-lg-2">
                <button name="circulo" class="btn btn-primary col-lg-8">Circulo</button>
            </div>
        </div>
    </div>

</form>

