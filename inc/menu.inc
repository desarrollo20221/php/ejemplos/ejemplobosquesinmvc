<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
        <a class="navbar-brand" href="index.php">Ejemplo Bosques sin MVC</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-link active" aria-current="page" href="index.php">Bosques</a>
                <a class="nav-link" href="playas.php">Playas</a>
                <a class="nav-link" href="operaciones.php">Operaciones Basicas</a>
                <a class="nav-link" href="dibujar.php">Dibujar</a>
            </div>
        </div>
    </div>
</nav>
