
<form>
    <div class="container-fluid">
        <div class="row pt-3">
            <div class="col-3">
                <label for="numwero1" class="form-label">Numero 1:</label>
            </div>

            <div class="col-9">
                <input type="number" class="form-control" id="correo" placeholder="Introduce un numero" name="numeros[]">
            </div>
        </div>
        <div class="row pt-3">
            <div class="col-3">
                <label for="numero2" class="form-label">Numero 2:</label>
            </div>

            <div class="col-9">
                <input type="number" class="form-control" id="contra" placeholder="Introduce un numero" name="numeros[]">

            </div>
        </div> 
        <div class="row pt-3">
            <div class="col-3">
                <label for="numero3" class="form-label">Numero 3:</label>
            </div>

            <div class="col-9">
                <input type="number" class="form-control" id="contra" placeholder="Introduce un numero" name="numeros[]">

            </div>
        </div>
        <div class="form-group row mt-2">
            <div class="col-lg-2">
                <button name="suma" class="btn btn-primary col-lg-8">Sumar</button>
            </div>
            <div class="col-lg-2">
                <button name="maxi" class="btn btn-primary col-lg-8">Maximo</button>
            </div>
            <div class="col-lg-2">
                <button name="mini" class="btn btn-primary col-lg-8">Minimo</button>
            </div>
            <div class="col-lg-2">
                <button name="media" class="btn btn-primary col-lg-8">Media</button>
            </div>
        </div>
    </div>

</form>